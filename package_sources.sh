#!/bin/bash

VERSION=1.0

pushd sources
tar cvJf ../gnome-shell-theme-deskos-${VERSION}.tar.xz --dereference gnome-shell-theme-deskos-${VERSION}
popd
